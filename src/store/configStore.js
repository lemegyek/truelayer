import {createStore} from "redux";
import banks from "../reducers/banks";

// Create store with optionally firing up redux dev tools
const Store = window.__REDUX_DEVTOOLS_EXTENSION__ ? createStore(banks, window.__REDUX_DEVTOOLS_EXTENSION__()) : createStore(banks);

export default Store;