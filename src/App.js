import React, {Component} from 'react';
import './App.css';

class App extends Component {
	render() {
		return (
			<div className="App">
				<p className="App-intro">
					Intro
				</p>
				{this.props.children}
			</div>
		);
	}
}

export default App;
