import _ from "lodash";

const banks = (state = [], action) => {
	switch (action.type) {
		case 'RECEIVE_ITEMS':
			return {
				...state,
				items: action.data
			};
		// console.log(action.type, state.items);
		case 'SELECT':
			// console.log(action.type, state.items, state.items.length);
			return {
				...state,
				selectedBank  : _.find(state.items,(item) => action.data.toLowerCase() === item.connectorId),
				searchedItems: _.filter(state.items, (entity) => _.includes(entity.name.toLowerCase(), action.data.toLowerCase()))
			};
		case 'RECEIVE_BANKS':
			return action.data;
		default:
			return state;
	}
};

export default banks;