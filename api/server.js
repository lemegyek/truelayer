var
	express     = require('express'),
	_           = require('lodash'),
	bodyParser  = require('body-parser'),
	cors        = require('cors'),
	app         = express(),
	log         = console.log,
	db          = require('./providers.json');

// bodyparser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// enable cors
app.use(cors({credentials: true, origin: true}));

// initialize router
var router = express.Router();

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// return message if empty
router.get('/', function (req, res) {
	res.send('please select an api url');
});

// get all data
router.get('/all', function (req, res) {
	res.send(db);
});

// get single item
router.get('/list/:id', function (req, res) {
	res.send(_.find(db, function(it){
		return it.connectorId === req.params.id;
	}));
});

// Start server and lo message
app.listen(3000);
console.log('Listening on localhost:3000');